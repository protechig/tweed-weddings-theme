<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, twd__scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tweed Weddings
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'twd__scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
