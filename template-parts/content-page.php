<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tweed Weddings
 */

?>
<div class="hero">
	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/border.svg'; ?>">
</div>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
			the_content();
			if( have_rows('page_sections')):
				while(have_rows('page_sections')): the_row();
					get_template_part('template-parts/content-blocks/block-' . get_row_layout() );
				endwhile;
			endif;

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tweed-weddings' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'tweed-weddings' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
