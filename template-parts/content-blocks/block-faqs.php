<?php
add_action( 'wp_footer', 'twd_faqs_js', 999);
function twd_faqs_js() {
    ?>
    <script>
    jQuery(document).ready(function() {
        jQuery('.trigger').click(function() {
            jQuery(this).siblings('.body').slideToggle();
            jQuery(this).toggleClass('active');
        })
    });
    </script>

    <?php
}

?>
<h2><?php the_sub_field('section_title'); ?></h2>
<?php
if( have_rows('faqs')):
    echo '<div class="faqs">';
    while( have_rows('faqs')): the_row();
    ?>
    <div class="accordion">
        <div class="trigger">
            <?php the_sub_field('question'); ?>
        </div>
        <div class="body">
            <?php the_sub_field('answer'); ?>
        </div>
    </div>
    <?php
    endwhile;
    echo '</div>';
endif;
