<?php
wp_enqueue_style( 'slick-carousel'  );
wp_enqueue_script( 'slick-carousel'  );
wp_enqueue_style( 'slick-carousel-theme' );
?>
<div class="accolades">
    <h2><?php the_sub_field('accolades_heading'); ?></h2>
    <div class="accolades-slider">
    <?php
        if(have_rows('accolades_slider')):
            while(have_rows('accolades_slider')) : the_row();
                echo '<img class="slide" src="' . get_sub_field('image') . '" >';
            endwhile;
        endif;
    ?>
    </div>
</div>
<?php
add_action('wp_footer', 'twd_accolades', 100);
function twd_accolades() {
?>
<script>
jQuery('.accolades-slider').slick({
  dots: false,
  infinite: true,
  autoplay: true,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
console.log('slick');

</script>
<?php }
