<div class="two-buttons">

    <a class="button blue" href="<?php the_sub_field('left_button_link'); ?>" ><?php the_sub_field('left_button_text'); ?></a>
    <a class="button green" href="<?php the_sub_field('right_button_link'); ?>" ><?php the_sub_field('right_button_text'); ?></a>
</div>
