<?php
wp_enqueue_style( 'slick-carousel'  );
wp_enqueue_script( 'slick-carousel'  );
wp_enqueue_style( 'slick-carousel-theme' );
?>
<div class="section testimonials" style="background: #829ba9" >
    <div class="wrap">
        <div class="testimonial-carousel">
        <?php if( have_rows('testimonials_list') ): ?>
            <?php while( have_rows('testimonials_list') ) : the_row(); ?>
                    <div class="testimonial">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg/quote.svg'; ?>" >
                        <div class="quote">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg/stars.svg'; ?>" >
                            <p><?php the_sub_field('testimonial_content'); ?></p>
                        </div>
                    </div>
            <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </div>
</div>
<?php
add_action('wp_footer', 'twd_accolades', 100);
function twd_accolades() {
?>
<script>
jQuery('.testimonial-carousel').slick({
  dots: false,
  infinite: true,
  autoplay: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
console.log('slick');

</script>
<?php }
