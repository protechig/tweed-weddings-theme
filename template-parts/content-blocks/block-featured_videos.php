<div class="section favorites">
    <h2><?php the_sub_field('section_title'); ?></h2>
    <div class="videos">
        <?php if ( function_exists( 'envira_gallery' ) ) { envira_gallery( get_sub_field('gallery_to_display') ); } ?>
    </div>
    <div class="button-group">
        <a href="<?php the_sub_field('more_favorites_link'); ?>" class="button green">More Favorites</a>
        <a target=_blank href="<?php the_sub_field('full_list_of_videos_link'); ?>" class="button blue">All Videos</a>
    </div>
</div>
