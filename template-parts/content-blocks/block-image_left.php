<div class="image-left">
    <div class="left-image">
        <img src="<?php the_sub_field('image'); ?>" />
    </div>
    <div class="right-text">
        <?php the_sub_field('content'); ?>
    </div>
</div>
