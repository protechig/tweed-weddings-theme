<div class="5050-split">
    <div class="left-half">
        <?php the_sub_field('left_half'); ?>
    </div>
    <div class="right-half">
        <?php the_sub_field('right_half'); ?>
    </div>
</div>
