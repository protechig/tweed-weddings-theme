<div class="section form" style="background: #829ba9" >
    <h2><?php the_sub_field('section_title'); ?></h2>
    <?php
        $form_object = get_sub_field('form');
        gravity_form_enqueue_scripts(1, true);
        gravity_form(1, false, false, false, '', false, 1);
    ?>
</div>