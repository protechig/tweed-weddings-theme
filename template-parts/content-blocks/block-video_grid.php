<div class="block-video-grid">
    <h2><?php the_sub_field('section_title'); ?></h2>
    <div class="videos">
        <?php if(have_rows('videos')): ?>
            <?php while(have_rows('videos')) : the_row(); ?>
                <div class="video">
                    <h3><?php the_sub_field('video_title'); ?></h3>
                    <?php the_sub_field('video_url'); ?>
                </div><!-- .video -->
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
