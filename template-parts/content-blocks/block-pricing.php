<div class="section pricing" style="background: url('<?php the_sub_field('background_image'); ?>');" >
    <div class="wrap">
        <div class="pricing-table">
            <div class="column-third">
                <h3>Basic</h3>
                <?php the_sub_field('basic_pricing_description'); ?>
                <p class="price"><?php the_sub_field('basic_pricing_cost'); ?></p>
                <a class="button" href=<?php the_sub_field('basic_pricing_link_url'); ?>>Learn More</a>
            </div>
            <div class="column-third">
                <h3>Premium</h3>
                <?php the_sub_field('premium_pricing_description'); ?>
                <p class="price"><?php the_sub_field('premium_pricing_cost'); ?></p>
                <a class="button" href=<?php the_sub_field('premium_pricing_url'); ?>>Learn More</a>
            </div>
            <div class="column-third">
                <h3>Elite</h3>
                <?php the_sub_field('elite_pricing_description'); ?>
                <p class="price"><?php the_sub_field('elite_pricing_cost'); ?></p>
                <a class="button" href=<?php the_sub_field('elite_pricing_page_url'); ?>>Learn More</a>
            </div>
        </div>
    </div>
</div>