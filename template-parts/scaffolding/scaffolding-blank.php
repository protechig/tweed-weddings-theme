<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Tweed Weddings
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'tweed-weddings' ); ?></h2>
</section>
