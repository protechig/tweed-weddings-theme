<?php
/**
 * Customizer sections.
 *
 * @package Tweed Weddings
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function twd__customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'twd__additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'tweed-weddings' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'twd__social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'tweed-weddings' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'tweed-weddings' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'twd__header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'tweed-weddings' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'twd__footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'tweed-weddings' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'twd__customize_sections' );
