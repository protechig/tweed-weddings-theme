<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tweed Weddings
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
		<?php
		if ( have_rows('content_blocks') ) :
			echo '<div id="fullpage">';
			/* Start the Loop */
			while ( have_rows('content_blocks') ) :
				the_row();

				/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
				get_template_part( 'template-parts/content-blocks/block', get_row_layout() );
				?>
						
					
				<?php
			endwhile;
		echo '</div>';

		endif;
		?>

		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
